<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Resource\Repository;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface FilterResourceAwareInterface
{
    /**
     * @param array $params
     * @param int   $perPage
     *
     * @return mixed
     */
    public function filter(array $params, int $perPage = 15);
}
