<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Resource\Repository;

use Inisiatif\Component\Contract\Resource\Model\ResourceInterface;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface HasLegacyIntranetRepositoryInterface
{
    /**
     * @param string|integer $intranetId
     *
     * @return ResourceInterface|null
     */
    public function findOneByIntranetId($intranetId): ?ResourceInterface;
}
