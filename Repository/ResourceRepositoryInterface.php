<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Resource\Repository;

use Inisiatif\Component\Contract\Resource\Model\ResourceInterface;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface ResourceRepositoryInterface
{
    /**
     * @param string|null $id
     *
     * @return ResourceInterface|null
     */
    public function findById(?string $id);

    /**
     * @param string $criteria
     * @param mixed  $value
     *
     * @return mixed
     */
    public function findBy(string $criteria, $value);
}
