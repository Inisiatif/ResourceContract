<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Resource\Model;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface SourceAwareInterface
{
    /**
     * @return string|null
     */
    public function getSource(): ?string;

    /**
     * @param string|null $value
     * @return SourceAwareInterface|self
     */
    public function setSource(?string $value);

    /**
     * @return string|null
     */
    public function getSourceId(): ?string;

    /**
     * @param string|null $value
     * @return SourceAwareInterface|self
     */
    public function setSourceId(?string $value);
}
