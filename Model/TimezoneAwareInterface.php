<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Resource\Model;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface TimezoneAwareInterface
{
    /**
     * @return string|null
     */
    public function getTimezone(): ?string;

    /**
     * @param string|null $value
     *
     * @return TimezoneAwareInterface|self
     */
    public function setTimezone(?string $value);
}
