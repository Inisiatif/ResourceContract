<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Resource\Model;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface BlameableAwareInterface
{
    /**
     * @return string|null
     */
    public function getCreatedBy(): ?string;

    /**
     * @param string|null $value
     *
     * @return BlameableAwareInterface|self
     */
    public function setCreatedBy(?string $value);

    /**
     * @return string|null
     */
    public function getUpdatedBy(): ?string;

    /**
     * @param string|null $value
     *
     * @return BlameableAwareInterface|self
     */
    public function setUpdatedBy(?string $value);
}
