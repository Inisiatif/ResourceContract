<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Resource\Model;

use DateTimeInterface;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface SoftDeleteableInterface
{
    /**
     * @return DateTimeInterface|null
     */
    public function getDeletedAt(): ?DateTimeInterface;

    /**
     * @param DateTimeInterface|null $date
     * @return SoftDeleteableInterface|self
     */
    public function setDeletedAt(?DateTimeInterface $date);
}
