<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Resource\Model;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface CodeAwareInterface
{
    /**
     * @return string|null
     */
    public function getCode(): ?string;

    /**
     * @param string|null $code
     * @return CodeAwareInterface|self
     */
    public function setCode(?string $code);
}
