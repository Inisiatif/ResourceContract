<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Resource\Model;

/**
 * @deprecated deprecated since version 3.2.2 use Inisiatif\Component\Contract\Currency\Model\CurrencyAwareInterface
 */
interface CurrencyAwareInterface
{
    /**
     * @return string|null
     */
    public function getCurrency(): ?string;

    /**
     * @param string|null $value
     *
     * @return CurrencyAwareInterface|self
     */
    public function setCurrency(?string $value): self;

    /**
     * @return float
     */
    public function getCurrencyRate(): float;

    /**
     * @param float $value
     *
     * @return CurrencyAwareInterface|self
     */
    public function setCurrencyRate(float $value = 1.00): self;
}
