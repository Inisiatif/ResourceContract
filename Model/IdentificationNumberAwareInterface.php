<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Resource\Model;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface IdentificationNumberAwareInterface
{
    /**
     * @return string|null
     */
    public function getIdentificationNumber(): ?string;

    /**
     * @param string|null $value
     * @return IdentificationNumberAwareInterface|self
     */
    public function setIdentificationNumber(?string $value);
}
