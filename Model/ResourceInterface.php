<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Resource\Model;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface ResourceInterface
{
    /**
     * @return integer|string|null
     */
    public function getId();

    /**
     * @param integer|string|null $id
     *
     * @return ResourceInterface|self
     */
    public function setId($id): self;
}
