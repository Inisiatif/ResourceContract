<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Resource\Model;

interface ReferenceAwareInterface
{
    public function getReference(): array;

    public function setReference(array $value);
}
