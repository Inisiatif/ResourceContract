<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Resource\Model;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface HashingIdAwareInterface
{
    /**
     * @return string
     */
    public function getHashId(): string;
}
