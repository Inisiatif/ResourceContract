<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Resource\Model;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface HasLegacyIntranetIdentifierInterface
{
    /**
     * @return string|integer
     */
    public function getIntranetId();

    /**
     * @param string|int|null $value
     *
     * @return HasLegacyIntranetIdentifierInterface|self
     */
    public function setIntranetId($value): self;
}
