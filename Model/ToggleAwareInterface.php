<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Resource\Model;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface ToggleAwareInterface
{
    /**
     * @param bool $value
     *
     * @return ToggleAwareInterface|self
     */
    public function setToggle(bool $value): self;

    /**
     * @return bool
     */
    public function getToggle(): bool;
}
