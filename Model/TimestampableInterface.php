<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Resource\Model;

use DateTimeInterface;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface TimestampableInterface
{
    /**
     * @return DateTimeInterface|null
     */
    public function getCreatedAt(): ?DateTimeInterface;

    /**
     * @param DateTimeInterface|null $createdAt
     * @return TimestampableInterface|self
     */
    public function setCreatedAt($createdAt);

    /**
     * @return DateTimeInterface|null
     */
    public function getUpdatedAt(): ?DateTimeInterface;

    /**
     * @param DateTimeInterface|null $createdAt
     * @return TimestampableInterface|self
     */
    public function setUpdatedAt($createdAt);
}
