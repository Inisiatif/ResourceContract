<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Resource\Model;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface TraceableInterface
{
    /**
     * @return string|null
     */
    public function getCreatedFrom(): ?string;

    /**
     * @param string|null $value
     *
     * @return TraceableInterface|self
     */
    public function setCreatedFrom(?string $value);

    /**
     * @return string|null
     */
    public function getUpdatedFrom(): ?string;

    /**
     * @param string|null $value
     *
     * @return TraceableInterface|self
     */
    public function setUpdatedFrom(?string $value);
}
