<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Resource\Util;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface CanonicalizerInterface
{
    /**
     * @param string|null $value
     * @return string|null
     */
    public static function canonicalize(?string $value): ?string;
}
