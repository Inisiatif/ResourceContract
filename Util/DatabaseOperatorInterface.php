<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Resource\Util;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface DatabaseOperatorInterface
{
    /**
     * @return string
     */
    public static function like(): string;
}
