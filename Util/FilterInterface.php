<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Resource\Util;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface FilterInterface
{
    /**
     * @param string|null $value
     *
     * @return mixed
     */
    public function apply(?string $value);
}
