<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Resource\Util;

use DateTimeInterface;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface CodeGeneratorInterface
{
    /**
     * @param string $code
     * @param DateTimeInterface|null $date
     * @return string
     */
    public function generate(string $code, ?DateTimeInterface $date = null): string;
}
