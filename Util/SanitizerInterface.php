<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Resource\Util;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface SanitizerInterface
{
    /**
     * @param string $value
     * @return string
     */
    public static function sanitize(string $value): string;

    /**
     * @param array $values
     * @return array
     */
    public static function sanitizeArray(array $values): array;
}
