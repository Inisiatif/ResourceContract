<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Resource\Util;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface FilterlableInterface
{
    /**
     * @param array $params
     *
     * @return mixed
     */
    public function build(array $params);
}
