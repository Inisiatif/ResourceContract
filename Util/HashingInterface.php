<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Resource\Util;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface HashingInterface
{
    /**
     * @param string|null $data
     * @return string|null
     */
    public static function encode($data): ?string;

    /**
     * @param string|int $data
     *
     * @return string|int
     */
    public static function decode($data);
}
