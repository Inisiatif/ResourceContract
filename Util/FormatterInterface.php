<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Resource\Util;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface FormatterInterface
{
    /**
     * @param mixed $value
     * @param string $format
     * @return string
     */
    public static function format($value, string $format): string;
}
