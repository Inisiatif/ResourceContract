<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Resource\Service;

use Inisiatif\Component\Contract\Resource\Model\ToggleAwareInterface;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface ToggleServiceAwareInterface
{
    /**
     * @param ToggleAwareInterface $resource
     *
     * @return bool
     */
    public function disable(ToggleAwareInterface $resource): bool;

    /**
     * @param ToggleAwareInterface $resource
     *
     * @return bool
     */
    public function enable(ToggleAwareInterface $resource): bool;
}
