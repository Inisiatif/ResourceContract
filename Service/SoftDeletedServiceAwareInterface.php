<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Resource\Service;

use Inisiatif\Component\Contract\Resource\Model\SoftDeleteableInterface;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface SoftDeletedServiceAwareInterface
{
    /**
     * @param SoftDeleteableInterface $resource
     *
     * @return bool
     */
    public function delete(SoftDeleteableInterface $resource): bool;

    /**
     * @param SoftDeleteableInterface $resource
     *
     * @return bool
     */
    public function restore(SoftDeleteableInterface $resource): bool;
}
